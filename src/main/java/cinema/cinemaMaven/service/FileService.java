package cinema.cinemaMaven.service;

import cinema.cinemaMaven.dto.FileData;
import cinema.cinemaMaven.dto.UserDataDto;

import java.util.List;

public interface FileService {

    FileData createFile(UserDataDto userDataDto, String path);

    List<FileData> findAll(String path);
}
