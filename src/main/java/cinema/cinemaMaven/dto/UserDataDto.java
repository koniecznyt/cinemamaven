package cinema.cinemaMaven.dto;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.FieldDefaults;
import cinema.cinemaMaven.classes.*;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Klasa odpowidzialna jest za przetrzymywanie podstawowych informacji o uzytkowniku.
 */
                                                    /* lombok: */
@Getter                                             /* automatyczne generowanie getterow dla pol */
@ToString                                           /* automatyczne generowanie metody toString */
@NoArgsConstructor                                  /* automatyczne generowanie konstruktora bezparametrowego */
@FieldDefaults(level = AccessLevel.PRIVATE)         /* automatyczne generowanie zasiegu dla pol */
public class UserDataDto implements Serializable {
    String firstName;
    String lastName;
    String email;
    Integer film_id;
    ArrayList<Integer> listOfReservedSeats;
    Integer price;

    public UserDataDto(String firstName, String lastName, String email, Integer film_id, ArrayList<Integer> listOfReservedSeats, Integer price) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.film_id = film_id;
        this.listOfReservedSeats = listOfReservedSeats;
        this.price = price;
    }

    public void whatFilmAndReservedSeats() throws Exception {
        if(this.film_id==1){
           // Film.film_kevin
            for(int i=0;i<this.listOfReservedSeats.size();i++){ //mniej iteracji (tyle ile wynosi ilosc miejsc zarezerwowanych przez usera)
                for(int j=0;j<Film.film_kevin.getReservedOrNot_array().size();j++) //zawsze 28
                    if(Film.film_kevin.getReservedOrNot_array().get(j).getId()==this.listOfReservedSeats.get(i)){
                        Film.film_kevin.getReservedOrNot_array().get(j).setStatus(1);//ustawia miejsce o numerze ktory jest zapisany w listofreservedseats na 1 czyli zarezerowane
                    }
            }
        }
        if(this.film_id==2){
           // Film.film_starwars
            for(int i=0;i<this.listOfReservedSeats.size();i++){ //mniej iteracji
                for(int j=0;j<Film.film_starwars.getReservedOrNot_array().size();j++) //zawsze 28
                    if(Film.film_starwars.getReservedOrNot_array().get(j).getId()==this.listOfReservedSeats.get(i)){
                        Film.film_starwars.getReservedOrNot_array().get(j).setStatus(1);//ustawia miejsce o numerze ktory jest zapisany w listofreservedseats na 1 czyli zarezerowane
                    }
            }

        }
        if(this.film_id==3){
           // Film.film_hp
            for(int i=0;i<this.listOfReservedSeats.size();i++){ //mniej iteracji
                for(int j=0;j<Film.film_hp.getReservedOrNot_array().size();j++) //zawsze 28
                    if(Film.film_hp.getReservedOrNot_array().get(j).getId()==this.listOfReservedSeats.get(i)){
                        Film.film_hp.getReservedOrNot_array().get(j).setStatus(1);//ustawia miejsce o numerze ktory jest zapisany w listofreservedseats na 1 czyli zarezerowane
                    }
            }

        }
        else {
            throw new Exception("blad, taki film nie istnieje");
        }
    }

}

