package cinema.cinemaMaven.model;

import cinema.cinemaMaven.classes.Film;
import cinema.cinemaMaven.dto.UserDataDto;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.Date;

@Component
public class DocumentComponentImpl implements DocumentComponent {

    private static final Logger LOGGER = LoggerFactory.getLogger(DocumentComponentImpl.class);
     private static Integer ticketId= 1001;
    @Override
    public void createDocument(UserDataDto userDataDto, String fileDestination) {
        try {
            ticketId++;
            Document document = new Document();
            PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(fileDestination));

            document.open();

            document.addTitle("Bilet Kino Tarnów ");
            document.addAuthor("Cinema-Tarnow");
            Paragraph preface = new Paragraph();
            Paragraph preface2 = new Paragraph();
            preface.add(new Paragraph("KINO TARNOW",new Font(Font.FontFamily.COURIER,20,Font.ITALIC)));
            addEmptyLine(preface,2);
            preface.add(new Paragraph("Bilet wygenerowany dla: "+userDataDto.getFirstName()+" "+userDataDto.getLastName()));
            addEmptyLine(preface,1);
            preface.add(new Paragraph("Tytul filmu: "+whatFilmById(userDataDto.getFilm_id()).getFilm_title()+" \nData: "+whatFilmById(userDataDto.getFilm_id()).getFilm_date()+" \nGodzina seansu: "+whatFilmById(userDataDto.getFilm_id()).getFilm_hour()));
            addEmptyLine(preface,2);
            document.add(preface);
            PdfPTable table = new PdfPTable(4);
            table.setWidths(new int[]{1, 1, 2, 2});
            table.addCell(createCell("Imie", 2, Element.ALIGN_LEFT));
            table.addCell(createCell("Nazwisko", 2, Element.ALIGN_LEFT));
            table.addCell(createCell("Adres email", 2, Element.ALIGN_LEFT));
            table.addCell(createCell("Zarezerwowane miejsca (numery)", 2, Element.ALIGN_LEFT));

            table.addCell(createCell(userDataDto.getFirstName(), 1, Element.ALIGN_LEFT));
            table.addCell(createCell(userDataDto.getLastName(), 1, Element.ALIGN_LEFT));
            table.addCell(createCell(userDataDto.getEmail(), 1, Element.ALIGN_LEFT));
            table.addCell(createCell(userDataDto.getListOfReservedSeats().toString(), 1, Element.ALIGN_LEFT));
            document.add(table);

            addEmptyLine(preface2,2);
            preface2.add(new Paragraph("Kwota do zaplaty: "+userDataDto.getPrice().toString()+" PLN"));
            addEmptyLine(preface2,1);
            preface2.add(new Paragraph("Przypominamy o koniecznosci uiszczenia oplaty 15 minut przed seansem. Zyczymy milego filmu!",new Font(Font.FontFamily.TIMES_ROMAN, 12,
                    Font.BOLD)));
            addEmptyLine(preface2,3);
            preface2.add(new Paragraph("Numer biletu: "+ticketId.toString()+ "-"+new Date()));
            document.add(preface2);


            setBackgroundAsGradient(document, writer);
            document.close();

        } catch (DocumentException | FileNotFoundException e) {
            LOGGER.error("i can't create document or file not exists");
        }
    }

    private PdfPCell createCell(String content, float borderWidth, int alignment) {
        final String FONT = "static/arial.ttf";

        Font font = FontFactory.getFont(FONT, BaseFont.IDENTITY_H, true);

        PdfPCell cell = new PdfPCell(new Phrase(content, font));
        cell.setBorderWidth(borderWidth);
        cell.setHorizontalAlignment(alignment);
        cell.setPaddingTop(3);
        cell.setPaddingBottom(6);
        cell.setPaddingLeft(3);
        cell.setPaddingRight(3);
        return cell;
    }

    private void setBackgroundAsGradient(Document document, PdfWriter writer) {
        Rectangle pageSize = document.getPageSize();
        PdfShading axial = PdfShading.simpleAxial(writer,
                pageSize.getLeft(pageSize.getWidth()/10), pageSize.getBottom(),
                pageSize.getRight(pageSize.getWidth()/10), pageSize.getBottom(),
                new BaseColor(100, 140, 160),
                new BaseColor(180, 160, 152), true, true);
        PdfContentByte canvas = writer.getDirectContentUnder();
        canvas.paintShading(axial);
    }

    private void addEmptyLine(Paragraph paragraph, int number){
        for(int i=0;i<number;i++){
            paragraph.add(new Paragraph(" "));
        }
    }

    private Film whatFilmById(int id){
        if(id == 1){
            return Film.film_kevin;
        }
        else if(id ==2){
            return Film.film_starwars;
        }
        else if (id==3){
            return Film.film_hp;
        }
        else return null;
    }
}
