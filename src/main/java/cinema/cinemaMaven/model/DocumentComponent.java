package cinema.cinemaMaven.model;

import cinema.cinemaMaven.dto.UserDataDto;

public interface DocumentComponent {
    void createDocument(UserDataDto userDataDto, String fileDestination);
}
