package cinema.cinemaMaven.rest;

import cinema.cinemaMaven.classes.*;
import cinema.cinemaMaven.dto.ErrorDto;
import cinema.cinemaMaven.dto.ErrorMessage;
import cinema.cinemaMaven.dto.FileData;
import cinema.cinemaMaven.dto.UserDataDto;
import cinema.cinemaMaven.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;



@Controller
@RequestMapping(value="/api")
public class ReservationApiController {
    @Autowired
    private FileService fileService;

    private static final Logger LOGGER = LoggerFactory.getLogger(ReservationApiController.class);
    private static final String PATH = "/Users/tkonieczny/Downloads/";

    @CrossOrigin
    @RequestMapping(value="/reservation/add", method = RequestMethod.POST)
    public ResponseEntity<UserDataDto> makeNewReservation(@RequestBody UserDataDto userDataDto){
       try{
           userDataDto.whatFilmAndReservedSeats();
       }catch (Exception er1){
           System.out.println("Error in /reservation/add POST");
           er1.printStackTrace();
       }
        return new ResponseEntity<>(userDataDto,HttpStatus.CREATED);
    }

    @CrossOrigin
    @RequestMapping(value = "/data/film/get/id/{id}", method = RequestMethod.GET)
    public ResponseEntity<Film> getFilmData(@PathVariable("id") Integer film_id){
        if(film_id==1) return new ResponseEntity<Film>(Film.film_kevin,HttpStatus.OK);
        if(film_id==2) return new ResponseEntity<Film>(Film.film_starwars,HttpStatus.OK);
        if(film_id==3) return new ResponseEntity<Film>(Film.film_hp,HttpStatus.OK);
        else return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @CrossOrigin
    @PostMapping(value = "/reservation/print/ticket")
    public ResponseEntity<?> createFile(@RequestBody UserDataDto userDataDto) {
        LOGGER.info("--- create pdf file for user: {}", userDataDto.getFirstName());

        FileData fileData = fileService.createFile(userDataDto, PATH);

        ErrorDto errorMessage = new ErrorDto(ErrorMessage.ERROR_PATH.getErrorMessage());
        HttpStatus errorCode = ErrorMessage.ERROR_PATH.getErrorCode();

        return fileData != null ?
                new ResponseEntity<>(fileData, HttpStatus.CREATED) :
                new ResponseEntity<>(errorMessage, errorCode);
    }

}


