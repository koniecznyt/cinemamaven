package cinema.cinemaMaven.classes;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.ToString;
import lombok.experimental.FieldDefaults;

import java.util.ArrayList;

@ToString
@Getter
@FieldDefaults(level = AccessLevel.PRIVATE)


public class Film {
    int film_id;
    String film_title;
    String film_date;
    String film_hour;
    ArrayList<Seats> reservedOrNot_array;


    public Film(int film_id, String film_title, String film_date, String film_hour) {
        this.film_id = film_id;
        this.film_title = film_title;
        this.film_date = film_date;
        this.film_hour = film_hour;
        this.reservedOrNot_array=useDefaultReservedArray(); // automatycznie ustawia wszytskie miejsca na wolne na dany seans
    }

    public ArrayList<Seats> useDefaultReservedArray(){  //wszytskie miejsca wolne (status =0)
        ArrayList<Seats> defaultReservedArray = new ArrayList<>();
        for(int i=1;i<=28;i++){
            defaultReservedArray.add(new Seats(i,0));
        }
        return defaultReservedArray;
    }




    public static Film film_kevin = new Film(1,"Kevin sam w domu","12.02.2020","18:00");
    public static Film film_starwars= new Film(2,"Gwiezdne wojny: Skywalker. Odrodzenie","15.02.2020","21:30");
    public static Film film_hp= new Film(3,"Harry Potter i Insygnia Smierci","20.02.2020","19:45");


}
