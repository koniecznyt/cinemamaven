package cinema.cinemaMaven.classes;

public class Seats {
    private Integer id;
    private Integer status; //0 = wolne , 1=zajete

    public Seats(Integer id, Integer status) {
        this.id = id;
        this.status = status;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}


